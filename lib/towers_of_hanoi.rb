# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_reader :towers

  def initialize()
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts render
    until won?
      puts "Which tower would you like to move from?"
      from_tower = gets.to_i

      puts "Which tower would you like to move to?"
      to_tower = gets.to_i

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        puts render
      else
        puts "Invalid move. Please try again."
      end
    end
    puts "You win!"
  end

  def render
    tower_0, tower_1, tower_2 = @towers[0], @towers[1], @towers[2]
    puts "Tower 0: "
    if tower_0.empty?
      render_empty_tower
    else
      render_tower(tower_0)
    end
    puts "Tower 1:"
    if tower_1.empty?
      render_empty_tower
    else
      render_tower(tower_1)
    end
    puts "Tower 2:"
    if tower_2.empty?
      render_empty_tower
    else
      render_tower(tower_2)
    end
  end

  def render_tower(tower)
    if tower.length == 1
      puts "    |"
      puts "    |"
      puts (tower[0].times { print "   ___" })
    elsif tower.length == 2
      puts "    |"
      puts (tower[-1].times { print " ___" })
      puts (tower[0].times { print "___" })
    else
      puts (tower[-1].times { print "   ___" })
      puts (tower[-2].times { print " ___" })
      puts (tower[0].times { print "___" })
    end
  end

  def render_empty_tower
    puts "    |"
    puts "    |"
    puts "    |"
  end

  def won?
    @towers == [[], [3, 2, 1], []] || @towers == [[], [], [3, 2, 1]]
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    if @towers[from_tower][-1] > @towers[to_tower][-1]
      return false
    end
    true
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new().play
end
